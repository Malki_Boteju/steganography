﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.IO;

namespace Steganography
{
    public partial class Steganography : Form
    {
        private Bitmap bmp = null;
        private Bitmap stegnagraphedBmp = null;
        private string extractedText = string.Empty;

        public Steganography()
        {
            InitializeComponent();
        }


        private void hideButton_Click(object sender, EventArgs e)
        {
            bmp = (Bitmap)imagePictureBox.Image;

            string text = dataTextBox.Text;
            string message;

            if (bmp == null)
            {
                MessageBox.Show("Choose an image", "Warning");

                return;
            }

            else if (text.Equals(""))
            {
                MessageBox.Show("The text you want to hide can't be empty", "Warning");

                return;
            }



            stegnagraphedBmp = SteganographyHandler.hideText(text, bmp);
            message = SteganographyHandler.checkStorage(bmp,text);
        



            if (message == "Your text was hidden in the image successfully!")
            {
                MessageBox.Show(message, "Done");
                notesLabel.Text = "Notes: don't forget to save your new image.";

               
                button2.Enabled = true;

                pictureBox2.Image = (Image)stegnagraphedBmp;

                notesLabel.ForeColor = Color.OrangeRed;
            }
            else {
                MessageBox.Show(message, "Warning");
                return;
            }

    
        }

        private void extractButton_Click(object sender, EventArgs e)
        {
            bmp = (Bitmap)imagePictureBox.Image;

            string extractedText = SteganographyHandler.extractText(bmp);


            textBox2.Text = extractedText;
        }

        private void imageToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            hideButton.Enabled = true;
            OpenFileDialog open_dialog = new OpenFileDialog();
            open_dialog.Filter = "Image Files (*.jpeg; *.png; *.bmp)|*.jpg; *.png; *.bmp";

            if (open_dialog.ShowDialog() == DialogResult.OK)
            {
                imagePictureBox.Image = Image.FromFile(open_dialog.FileName);

                if (imagePictureBox.Image != null) {

                    pictureBox2.BackgroundImage = null;
                }
                
            }
        }

        private void imageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog save_dialog = new SaveFileDialog();
            save_dialog.Filter = "Png Image|*.png|Bitmap Image|*.bmp";

            if (save_dialog.ShowDialog() == DialogResult.OK)
            {
                switch (save_dialog.FilterIndex)
                {
                    case 0:
                        {
                            bmp.Save(save_dialog.FileName, ImageFormat.Png);
                        }break;
                    case 1:
                        {
                            bmp.Save(save_dialog.FileName, ImageFormat.Bmp);
                        } break;
                }


                notesLabel.Text = "Notes:";
                notesLabel.ForeColor = Color.Black;
            }
        }

        private void textToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog save_dialog = new SaveFileDialog();
            save_dialog.Filter = "Text Files|*.txt";

            if (save_dialog.ShowDialog() == DialogResult.OK)
            {
                File.WriteAllText(save_dialog.FileName, dataTextBox.Text);
            }
        }

        private void textToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            OpenFileDialog open_dialog = new OpenFileDialog();
            open_dialog.Filter = "Text Files|*.txt";

            if (open_dialog.ShowDialog() == DialogResult.OK)
            {
                dataTextBox.Text = File.ReadAllText(open_dialog.FileName);
            }
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("By: Hamzeh Soboh © 2013", "About");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog open_dialog = new OpenFileDialog();
            open_dialog.Filter = "Image Files (*.jpeg; *.png; *.bmp)|*.jpg; *.png; *.bmp";

            if (open_dialog.ShowDialog() == DialogResult.OK)
            {
                imagePictureBox.Image = Image.FromFile(open_dialog.FileName);
            }
        }

        private void dataTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            SaveFileDialog save_dialog = new SaveFileDialog();
            save_dialog.Filter = "Png Image|*.png|Bitmap Image|*.bmp";
            save_dialog.FileName = textBox1.Text;

 

            if (save_dialog.ShowDialog() == DialogResult.OK)
            {
                switch (save_dialog.FilterIndex)
                {
                    case 0:
                        {
                            stegnagraphedBmp.Save(save_dialog.FileName, ImageFormat.Png);
                        }
                        break;
                    case 1:
                        {
                            stegnagraphedBmp.Save(save_dialog.FileName, ImageFormat.Bmp);
                        }
                        break;
                }

                notesLabel.Text = "Notes:";
                notesLabel.ForeColor = Color.Black;
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void notesLabel_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            OpenFileDialog open_dialog = new OpenFileDialog();
            open_dialog.Filter = "Image Files (*.jpeg; *.png; *.bmp)|*.jpg; *.png; *.bmp";

           

                if (open_dialog.ShowDialog() == DialogResult.OK)
                {
                pictureBox1.Image = Image.FromFile(open_dialog.FileName);
              
            }
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            bmp = (Bitmap)pictureBox1.Image;

            if (bmp == null)
            {
                MessageBox.Show("Please choose a image", "Warning");

                return;
            }
            else if (SteganographyHandler.extractText(bmp)=="") {
                MessageBox.Show("The image has no secret message.\nPlease choose another image", "Warning");
            }
            else
            {
                string extracted = SteganographyHandler.extractText(bmp);
                textBox2.Text = extracted;
            }

            
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

    

        private void imagePictureBox_Click(object sender, EventArgs e)
        {
           
        }
    }
}