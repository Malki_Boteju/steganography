﻿using System;
using System.Drawing;

namespace Steganography
{
    class SteganographyHandler
    {
        public enum State
        {
            Setting_To_Zero,
            Fill_Tail_Zeros
        };


        //hide the message
        public static Bitmap hideText(string text, Bitmap bmp)
        {
            
            State state = State.Setting_To_Zero;
            
            int charIndex = 0;
            
            int charValue = 0;
            
            int pixelIndex= 0;
            
            int zeros = 0;
            
            int R = 0, G = 0, B = 0;
            

            for (int i = 0; i < bmp.Height; i++)
            {
                for (int j = 0; j < bmp.Width; j++)
                {
                    Color pixel = bmp.GetPixel(j, i);
                    
                    //Removing the least significant bit from each channel in a pixel
                    R = pixel.R - pixel.R % 2;
                    G = pixel.G - pixel.G % 2;
                    B = pixel.B - pixel.B % 2;
                    
                    for (int n = 0; n < 3; n++)
                    {
                        if (pixelIndex% 8 == 0)
                        {
                            if (state == State.Fill_Tail_Zeros && zeros == 8)
                            {
                                if ((pixelIndex- 1) % 3 < 2)
                                {
                                    bmp.SetPixel(j, i, Color.FromArgb(R, G, B));
                                }
                                
                                return bmp;
                            }
                            
                            //When there are no more characters to hide
                            if (charIndex >= text.Length)
                            {
                                state = State.Fill_Tail_Zeros;
                            }
                            else
                            {
                                charValue = text[charIndex++];
                            }
                        }
                        
                        switch (pixelIndex% 3)
                        {
                            case 0:
                                {
                                    if (state == State.Setting_To_Zero)
                                    {
                                        R += charValue % 2;
                                        charValue /= 2;
                                    }
                                } break;
                            case 1:
                                {
                                    if (state == State.Setting_To_Zero)
                                    {
                                        G += charValue % 2;
                                        charValue /= 2;
                                    }
                                } break;
                            case 2:
                                {
                                    if (state == State.Setting_To_Zero)
                                    {
                                        B += charValue % 2;
                                        charValue /= 2;
                                    }

                                    
                                    bmp.SetPixel(j, i, Color.FromArgb(R, G, B));
                                } break;
                        }

                        pixelIndex++;

                        if (state == State.Fill_Tail_Zeros)
                        {
                            zeros++;
                        }
                    }
                }
            }

            return bmp;
        }




        // find whether there is suffcient space
            public static string checkStorage(Bitmap bmp, string message)
        {

            string alert = "Your text was hidden in the image successfully!";
            if (message.Length > 255)
            {
                alert = "The message is too long.It should contain no more than 255 characters";
            }

            return alert;
        }


        //extract the message
        public static string extractText(Bitmap bmp)
        {
            int colorUnitIndex = 0;
            int charValue = 0;
            
            string extractedText = String.Empty;
            
            for (int i = 0; i < bmp.Height; i++)
            {
                for (int j = 0; j < bmp.Width; j++)
                {
                    Color pixel = bmp.GetPixel(j, i);
                    
                    for (int n = 0; n < 3; n++)
                    {
                        switch (colorUnitIndex % 3)
                        {
                            case 0:
                                {
                                    charValue = charValue * 2 + pixel.R % 2;
                                } break;
                            case 1:
                                {
                                    charValue = charValue * 2 + pixel.G % 2;
                                } break;
                            case 2:
                                {
                                    charValue = charValue * 2 + pixel.B % 2;
                                } break;
                        }

                        colorUnitIndex++;
                        
                        if (colorUnitIndex % 8 == 0)
                        {

                            charValue = reverseBits(charValue);

                            if (charValue >127)
                            {
                                return "";
                            }
                            
                           if (charValue == 0)
                            {
                                return extractedText;

                            }
                            char c = (char)charValue;
                            extractedText += c.ToString();
                        }
                    }
                }
            }

            return extractedText;
        }

        //To reverse the extracted chunks of 8 bits -----> to get the correct character
        public static int reverseBits(int n)
        {
            int result = 0;

            for (int i = 0; i < 8; i++)
            {
                result = result * 2 + n % 2;

                n /= 2;
            }

            return result;
        }
    }
}
